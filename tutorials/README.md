# Tutorials

- Basics of programming
- Overview of data management
- Advanced data management (in development, see the branch dev_advanced_data_management)
- Graphics (in development, see the branch dev_graphics)
- Shiny (in development, see the branch dev_shiny)
- Text mining (in development, see the branch dev_text_mining)
- Cartography (in development, see the branch dev_carto)
- Sankey Diagram