
| File 		| Description 	| 
| -------- 	| -------- 		| 
| data_sankey_100_patients.csv      	| Synthetic dataset with containing 100 patients and 3 steps per patient     		| 
| generation_of_synthetic_dataset.R     | R script for generating a synthetic dataset dedicated to the Sankey diagram  		| 
| sankey_tuto_data_2.csv     	| Synthetic dataset for a future tutorial on the Sankey diagram with advanced challenges	| 
| sankey_tutorial_data.csv     	| First version, manually generated, of a synthetic dataset for the Sankey diagram	| 